export const MENU_LINKS = [
	{
		title: "Home",
		path: "/",
	},
	{
		title: "About",
		path: "/about/",
	},
	{
		title: "Blog",
		path: "/posts/",
	},
];

// ! Remember to add your own socials
export const SOCIAL_LINKS = {
	github: "https://codeberg.org/TheHolyTachanka",
	twitter: "https://fosstodon.org/@TheOneSlav",
	discord: "https://discord.gg/A6bzhX5VQk",
	email: "me@theholytachanka.ml",
};
